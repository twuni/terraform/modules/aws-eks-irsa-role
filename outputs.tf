output "role_name" {
  description = "The name of the IAM role mapped to the given Kubernetes service account."
  value       = aws_iam_role.irsa.name
}

output "user_name" {
  description = "The name of the IAM user equivalent of the IRSA role."
  value       = aws_iam_user.portable.name
}
