# IAM Role for Service Account (IRSA) | AWS | Terraform Modules | Twuni

This Terraform module provisions an IAM role mapped to a Kubernetes service account.

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.0.8 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 3.60.0 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_name"></a> [name](#input\_name) | The name of the Kubernetes service account. | `string` | `"default"` | no |
| <a name="input_namespace"></a> [namespace](#input\_namespace) | The Kubernetes namespace in which the service account is provisioned. | `string` | `"default"` | no |
| <a name="input_oidc_provider_arn"></a> [oidc\_provider\_arn](#input\_oidc\_provider\_arn) | The ARN of the EKS cluster's OpenID Connect provider. | `string` | n/a | yes |
| <a name="input_oidc_provider_url"></a> [oidc\_provider\_url](#input\_oidc\_provider\_url) | The URL of the EKS cluster's OpenID Connect provider. | `string` | n/a | yes |
| <a name="input_tags"></a> [tags](#input\_tags) | Tags to be set on all resources provisioned by this module. | `map(string)` | `{}` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_role_name"></a> [role\_name](#output\_role\_name) | The name of the IAM role mapped to the given Kubernetes service account. |
| <a name="output_user_name"></a> [user\_name](#output\_user\_name) | The name of the IAM user equivalent of the IRSA role. |
