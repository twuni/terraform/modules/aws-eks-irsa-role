resource "aws_iam_role" "irsa" {
  name_prefix        = "${local.slug}-irsa-"
  assume_role_policy = data.aws_iam_policy_document.arp.json

  tags = merge(var.tags, {
    "app.kubernetes.io/namespace" = var.namespace
    "app.kubernetes.io/name"      = var.name
    "net.openid/connect/provider" = var.oidc_provider_url
    Name                          = "${var.name}-irsa"
  })
}

resource "aws_iam_user" "portable" {
  name = aws_iam_role.irsa.name

  tags = merge(var.tags, {
    "app.kubernetes.io/namespace" = var.namespace
    "app.kubernetes.io/name"      = var.name
    "net.openid/connect/provider" = var.oidc_provider_url
    Name                          = "${var.name}-irsa"
  })
}
