locals {
  slug = substr(sha256("${var.oidc_provider_url}-${var.namespace}-${var.name}"), 0, 8)
}
