variable "name" {
  default     = "default"
  description = "The name of the Kubernetes service account."
  type        = string
}

variable "namespace" {
  default     = "default"
  description = "The Kubernetes namespace in which the service account is provisioned."
  type        = string
}

variable "oidc_provider_arn" {
  description = "The ARN of the EKS cluster's OpenID Connect provider."
  type        = string
}

variable "oidc_provider_url" {
  description = "The URL of the EKS cluster's OpenID Connect provider."
  type        = string
}

variable "tags" {
  default     = {}
  description = "Tags to be set on all resources provisioned by this module."
  type        = map(string)
}
