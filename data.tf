data "aws_caller_identity" "current" {
}

data "aws_iam_policy_document" "arp" {
  statement {
    actions = ["sts:AssumeRoleWithWebIdentity"]
    effect  = "Allow"

    condition {
      test     = "StringEquals"
      values   = ["system:serviceaccount:${var.namespace}:${var.name}"]
      variable = "${replace(var.oidc_provider_url, "https://", "")}:sub"
    }

    principals {
      identifiers = [var.oidc_provider_arn]
      type        = "Federated"
    }
  }
}
